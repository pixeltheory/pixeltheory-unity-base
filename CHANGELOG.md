# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.0] - 2021.02.01
### ADDED
- Unity testing package.
- Uunit tests.
- Added pixeltheory-unity-debug package contents.
- Added refactored versions of Editor and Runtime Logger classes.
### CHANGED
- Changed logging calls to use new internal Logger functions.
- Changed layout of package to be more consistent with Unity standards.
- Refactoring naming throughout project to be more concise and consistent.
- Changed input system to new InputSystem package.
- Changed package description in package.json.
### REMOVED
- Unused packages (Unity UI, Animation, etc.).

## [1.3.0] - 2020.04.22
### CHANGED
- Names of singleton classes have 'Singleton' at the end of their class names now.
### ADDED
- PixeltheoryScriptableObject class.
- PixeltheoryScriptableObjectSingleton class.
### REMOVED
- PixeltheoryRuntimeData. Redundant now that we have PixeltheoryScriptableObjectSingleton.

## [1.2.6] - 2020.02.17
### CHANGED
- Clean up of outdated comments.
- Removed overused debug logs.

## [1.2.5] - 2019.08.07
### CHANGED
- Upgraded package Unity project to Unity 2019.2.0f1.
- Added ScriptableObject runtime data base class (PixeltheoryRuntimeData).
- Commented out MonoBehaviour virtual method implementations in PixeltheoryBehaviour.

## [1.2.4] - 2019.07.15
### FIXED
- Removed calls to PixeltheoryDebug in PixeltheorySingletonBehaviour.Awake code.
### CHANGED
- Updated README.md to reflect the subtraction of debug code.
- Moved ScriptOrder attribute out of Pixeltheory.Attributes namespace and into Pixeltheory.
- Moved ScriptOrderPreprocessor editor script out of Pixeltheory.Attributes namespace and into Pixeltheory.Editor. 

## [1.2.3] - 2019.07.15
### FIXED
- Added 'UNITY_EDITOR' conditional to PixeltheoryDebug runtime logging.

## [1.2.2] - 2019.07.13
### CHANNGED
- Changed the access modifiers of the runtime logging methods in PixeltheoryDebug to 'internal'.
- Changed the names of the runtime logging methods in PixeltheoryDebug to have the postfix 'Internal'.

## [1.2.1] - 2019.07.08
### FIXED
- Infinite singleton creation loop caused by copy paste code fixed  to use first created as protected instance.

## [1.2.0] - 2019.06.29
### ADDED
- Added Editor only versions of Log, Warning, and Error in PixeltheoryDebug
### CHANGED
- Changed the names of runtime versions of Log, Warning, and Error to RuntimeLog, etc.
- Refactored and added debug log calls throughout the package.

## [1.1.0] - 2019.06.27
### CHANGED
- Separated classes in the same file into individual files.

## [1.0.0] - 2019.06.23
### CHANGED
- Moved PixeltheoryBehaviour and PixeltheorySingletonBehaviour to Pixeltheory namespace.
- Moved Pixeltheory debugging singleton logger to Pixeltheory.Debug namespace.
- Changed name of Runtime/Scripts/Debugger folder to Runtime/Scripts/Debug.
### REMOVED
- Removed 'v' prefix from versioning numbers to be fully compliant with SemVer 2.0.
### ADDED
- Added callback to script recompile/reload editor event to re-preprocess ScriptOrder attributes.

## [0.0.4] - 2019.06.18
### FIXED
- Changed access modifier of privateInstance to protected.
- Changed name of privateInstance to protectedInstance.

## [0.0.3] - 2019.06.16
### ADDED
- Added PixeltheoryBehaviour.
- Added PixeltheorySingletonBehaviour.
- Added ScriptOrder attribute and attribute preprocessor.
- Added Unity project.
- Added asm file for package.

## [0.0.2] - 2019.06.13
### FIXED
- Updated .gitignore.
- Updated .gitattributes.


## [0.0.1] - 2019.06.09
### Added
- Added AmplifyShaderEditor folder rule to .gitignore.
- Added .vscode rule to .gitignore.
- Commented out .hgignore, .hgtags, .apk, and .unitypackage.

## [0.0.0] - 2019.06.09
### Added
- Added README.md file.
- Added CHANGELOG.md file.
- Added .gitattributes file.
- Added .gitconfig file.
- Added .gitignore file.
