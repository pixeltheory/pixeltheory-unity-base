# Pixeltheory Unity Base Classes and Scripts #

Classes and scripts that can be used in all Pixeltheory Unity projects.



### Instructions ###

Paste the below text into the dependencies section of your Packages/manifest.json:

> "com.pixeltheory.base" : "ssh://git@gitlab.com/pixeltheory/pixeltheory-unity-base.git#package"

In your code, use the following namespaces to refrence the base classes in this package:

> using Pixeltheory;
> using Pixeltheory.Debug;
> using Pixeltheory.Editor;
> using Pixeltheory.Editor.Debug;



### FAQ ###

Q : How do I refresh/reload the package?
A : As of 2019.1.X, the package manager doesn't have a way to refresh/reload Git based packages. You have to open Packages/manifest.json and delete the "lock" section to have Unity refresh/reload the "locked" Git packages.

Q : If another package depends on this one, will the dependency be downloaded automatically?
A : No. Unity will only allow Unity hosted official packages to be downloaded as a dependency. This is due to security reasons. Unofficial packages like Pixeltheory packages, need to have dependencies explicitly added to the packages manifest.



### Who do I talk to? ###

If you have any questions or problems with the files in this repository, please email contact@ellistalley.com.
