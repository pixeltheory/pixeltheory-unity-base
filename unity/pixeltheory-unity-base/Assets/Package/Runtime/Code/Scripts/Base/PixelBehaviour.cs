﻿using UnityEngine;


namespace Pixeltheory
{
    public class PixelBehaviour : MonoBehaviour
    {
        /*  
            This class implemention is reserved for future
            use, but can still be inherited from currently.
        */
    }
}
