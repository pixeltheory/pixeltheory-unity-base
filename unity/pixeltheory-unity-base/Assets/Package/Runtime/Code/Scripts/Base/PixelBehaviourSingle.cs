﻿using UnityEngine;
using Pixeltheory.Debug;


namespace Pixeltheory
{
    /*  
        This single class only insures that there is one object of this component/type
        during runtime. It creates an instance if none exists. Note that this is NOT
        a singleton, as there is no getInstance style getter, and because it is generic,
        inheritance is not a problem.
    */
    public class PixelBehaviourSingle<T> : PixelBehaviour where T : PixelBehaviour
    {
        #region Class
        #region Fields
        #region Protected
        protected static T instance;
        #endregion //Protected
        
        #region Private
        private static bool shuttingDown = false; 
        #endregion //Private
        #endregion //Fields
        #endregion //Class

        #region Instance
        #region Methods
        #region Unity Messages
        protected virtual void Awake()
        {
            string fullName = typeof(T).FullName;
            if (PixelBehaviourSingle<T>.instance == null && !PixelBehaviourSingle<T>.shuttingDown)
            {
                // First instance of this PixelBehaviourSingle, so we set ourselves as the protected instance
                PixelBehaviourSingle<T>.instance = this as T;
                Logging.Log("[{0}] Setting first instance as single instance.", fullName);
            }
            else
            {
                GameObject.Destroy(this as UnityEngine.Object);
                Logging.Warn("[{0}] Instance already exists; destroying self.", fullName);
            }
        }

        protected virtual void OnDestroy()
        {
            if (PixelBehaviourSingle<T>.instance == this as T && !PixelBehaviourSingle<T>.shuttingDown)
            {
                PixelBehaviourSingle<T>.instance = null;
                PixelBehaviourSingle<T>.shuttingDown = true;
                Logging.Log("[{0}] Singleton instance is being destroyed.", typeof(T).FullName);
            }
        }
        #endregion //Unity Messages
        #endregion //Methods
        #endregion //Instance
    }
}
