﻿using System;
using UnityEngine;


namespace Pixeltheory
{
    [Serializable]
    public class PixelScriptableObject : ScriptableObject
    {
        /*  
            This class implemention is reserved for future
            use, but can still be inherited from currently.
        */
    }   
}
