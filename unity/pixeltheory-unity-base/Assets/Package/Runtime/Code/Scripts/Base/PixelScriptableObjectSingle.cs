﻿using UnityEngine;
using Pixeltheory.Debug;


namespace Pixeltheory
{
    public class PixelScriptableObjectSingle<T> : PixelScriptableObject where T : PixelScriptableObject
    {
        #region Class
        #region Fields
        #region Protected
        protected static T instance;
        #endregion //Protected
        
        #region Private
        private static bool shuttingDown = false;
        #endregion //Private
        #endregion //Fields
        #endregion //Class

        #region Instance
        #region Methods
        #region Unity Messages
        protected void Awake()
        {
            string fullName = typeof(T).FullName;
            if (PixelScriptableObjectSingle<T>.instance == null && !PixelScriptableObjectSingle<T>.shuttingDown)
            {
                PixelScriptableObjectSingle<T>.instance = this as T;
                Logging.Log("[{0}] Setting first instance as single instance.", fullName);
            }
            else
            {
                GameObject.Destroy(this as UnityEngine.Object);
                Logging.Warn("[{0}] Instance already exists; destroying self.", fullName);
            }
        }

        protected void OnDestroy()
        {
            if (PixelScriptableObjectSingle<T>.instance == this as T && !PixelScriptableObjectSingle<T>.shuttingDown)
            {
                PixelScriptableObjectSingle<T>.instance = null;
                PixelScriptableObjectSingle<T>.shuttingDown = true;
                Logging.Log("[{0}] Single instance being destroyed.", typeof(T).FullName);
            }
        }
        #endregion //Unity Messages
        #endregion //Methods
        #endregion //Instance
    }

}