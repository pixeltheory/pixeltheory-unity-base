﻿using System;
using UnityEngine;
using Pixeltheory.Debug;
using Object = UnityEngine.Object;


namespace Pixeltheory
{
    public class GameObjectKeepAlive : PixelBehaviour
    {
        #region Instance
        #region Methods
        #region Unity Messages
        protected virtual void Awake()
        {
            Object.DontDestroyOnLoad(this.gameObject);
            Logging.Log
            (
                "[{0}] Setting as persistent through scene loads/unloads.",
                typeof(GameObjectKeepAlive).FullName
            );
        }
        #endregion //Unity Messages
        #endregion //Methods
        #endregion //Instance
    }   
}