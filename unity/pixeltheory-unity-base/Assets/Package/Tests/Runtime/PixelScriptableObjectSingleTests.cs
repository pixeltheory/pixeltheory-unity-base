﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


namespace Pixeltheory.Tests
{
    public class PixelScriptableObjectSingleTests
    {
        [UnityTest]
        public IEnumerator RuntimeExistenceTest()
        {
            PixelScriptableObjectSingleTest testScriptableObjectOne =
                ScriptableObject.CreateInstance<PixelScriptableObjectSingleTest>();
            yield return null;
            PixelScriptableObjectSingleTest testScriptableObjectTwo =
                ScriptableObject.CreateInstance<PixelScriptableObjectSingleTest>();
            yield return null;
            Assert.IsTrue((testScriptableObjectOne != null) && (testScriptableObjectTwo == null));
        }

        private class PixelScriptableObjectSingleTest : PixelScriptableObjectSingle<PixelScriptableObjectSingleTest>
        {
            
        }
    }
}
